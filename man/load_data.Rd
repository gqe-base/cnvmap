% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/CNVmap_data_loading.R
\name{load_data}
\alias{load_data}
\title{Load data for CNVmap}
\usage{
load_data(mapFileName, segFileName, rawFileName = NULL,
  snpFileName = NULL)
}
\arguments{
\item{void}{}
}
\value{
list with two elements:\cr
$map data frame: each row is a marker (markers are ordered by chromosome 
and genetic position, columns are:
\itemize{
    \item $map$snpName character string: name of the marker
    \item $map$chr character string or numeric: chromosome (linkage group)
    \item $map$genPos numeric: genetic position in cM of the marker
}
$seg matrix: segregating genotypes coded as A, B, H, - Colnames: names 
of markers, rownames: names of individual in the population. Markers are 
ordered by chromosome and genetic position
}
\description{
Master function to load data for CNVmap (depending on files 
present in the working directory). Loads allele segregation data under one of 
the possible file formats, and genetic map position data. The names of files 
to be read are given by the function PAR() as a list containing the 
following elements:\cr
- SVmapFileName for mapping data, and\cr
- (SVrawFileName, SVsnpFileName or SVsegFileName, any one of these three files 
is sufficient) for segregation data.\cr
load_data keeps only markers present in both map and segregation files, and 
orders markers on chromosome and genetic position in both data sets.
}
\examples{
ldRet <- load_data()
seg <- ldRet$seg
map <- ldRet$map

}
